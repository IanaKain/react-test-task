import {urlToSearch} from '../utils';


const parseResponse = async response => {
  const contentType = response.headers.get('content-type');
  const jsonType = contentType && contentType.includes('application/json');
  const headers = response.headers.get('Link');
  const meta = {pagination: []};

  if (headers) {
    const pagination = headers
      .split(' ')
      .map(str => !str.includes('rel') && str.replace(/[<>;]/g, ''))
      .filter(Boolean)
      .map(urlToSearch);

    meta.pagination = [...new Set(pagination)];
  }

  let res = {meta};

  res.data =  jsonType ? await response.json() : await response.text();

  return res;
};

const onSuccess = response => {
  if (response.ok) {
    return parseResponse(response);
  } else {
    return parseResponse(response).then((data) => {
      throw new Error('api');
    })
  }
};

const combineRequest = (method, body) => {
  return method === 'GET' ? {method} : {...{method}, body: JSON.stringify(body)};
};

const connector = ({method, url, body = {}, headers}) => {
  const baseUrl = 'http://localhost:3000';

  return (
    fetch(`${baseUrl}${url}`, {...combineRequest(method, body), headers})
      .then(onSuccess)
      .catch(error => {
        throw error;
      })
  );
};

const api = {
  get: ({url}) => connector({method: 'GET', url}),
  post: ({url, body}) => connector({method: 'POST', headers: {'Content-Type': 'application/json'}, url, body}),
  delete: ({url}) => connector({method: 'DELETE', url}),
  patch: ({url, body}) => connector({method: 'PATCH', headers: {'Content-Type': 'application/json'}, url, body}),
};

export {api};
