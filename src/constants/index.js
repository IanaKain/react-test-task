const defaultLoader = Object.freeze({
  none: 'none',
  pending: 'pending',
  completed: 'completed',
  error: 'error',
});

const genders = {male: 'male', female: 'female', 'n/a': 'n/a'};

export {defaultLoader, genders};
