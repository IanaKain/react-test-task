const routes = {
  root: '/',
  characters: '/characters',
  createCharacter: '/characters/create',
  editCharacter: '/characters/edit/:id',
  species: '/species',
};

export {routes};