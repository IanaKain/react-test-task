import {compile} from 'path-to-regexp';


const generatePath = (path, params = {}) => compile(path)(params);

const urlToSearch = (url) => {
  const newUrl = new URL(url);
  return newUrl.search;
};

const searchToJSON = str => {
  const searchParams = new URLSearchParams(str);
  const urlParams = {};

  searchParams.forEach((value, key) => {
    const arr = searchParams.getAll(key);

    if (arr.length > 1) {
      urlParams[key] = arr;
    } else {
      urlParams[key] = searchParams.get(key);
    }
  });

  return urlParams;
};

const JSONToSearch = params => {
  const urlSearchParams = new URLSearchParams();

  Object.entries(params).forEach(([key, value]) => {
    if (value === undefined) {
      return;
    }

    if (Array.isArray(value)) {
      value.forEach(v => urlSearchParams.append(key, v));
    } else {
      urlSearchParams.set(key, value);
    }
  });

  return urlSearchParams.toString();
};

const debounce = (fn, ms) => {
  let timeout = null;

  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      fn(...args);
      timeout = null;
    }, ms);
  };
};

const capitalize = str => {
  if (!str) return null;

  const lowerCase = str.toLowerCase();

  return lowerCase.charAt(0).toUpperCase() + lowerCase.slice(1);
};

const noop = () => {};

export {urlToSearch, searchToJSON, JSONToSearch, debounce, capitalize, generatePath, noop};
