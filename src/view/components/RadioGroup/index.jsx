import React from 'react';
import PropTypes from 'prop-types';
import {capitalize} from '../../../utils';


const RadioGroup = (props) => {
    return (
      <div className="form-group">
        <label className="form-field-label">{capitalize(props.id)}</label>
        {props.required && <span className="required-mark">*</span>}
        {
          props.data.map((value) => {
            const isChecked = value === props.selected;

            return (
              <div key={value} className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name={value}
                  id={value}
                  value={value}
                  checked={isChecked}
                  onChange={props.onChange(props.id)}
                />
                <label className="form-check-label" htmlFor={value}>{capitalize(value)}</label>
                {props.required && <div className="invalid-feedback">{props.error}</div>}
              </div>
            );
          })
        }
      </div>
    );
}

RadioGroup.propTypes = {
  onChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  selected: PropTypes.string.isRequired,
  data: PropTypes.array,
  required: PropTypes.bool,
};

RadioGroup.defaultProps = {
  data: [],
  required: false,
};

export {RadioGroup};
