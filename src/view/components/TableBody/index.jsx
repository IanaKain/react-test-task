import React from 'react';
import PropTypes from 'prop-types';


const Td = ({idx, column}) => {
  const value = column.data[idx].target;
  const source = column.data[idx].source;

  return column.render
    ? <td>{column.render(value, source)}</td>
    : <td>{value}</td>;
};

const Th = ({idx, column}) => {
  const value = column.data[idx].target;
  const source = column.data[idx].source;

  return column.render
    ? <th scope="row">{column.render(value, source)}</th>
    : <th scope="row">{value}</th>;
};

const TableBody = (props) => (
  <tbody>
  {props.dataSource.map((source, sourceIdx) => {
    return (
      <tr key={source.id}>
        {props.columns.map((column, colIdx) => {
          return colIdx === 0
            ? <Th key={column.dataIndex} idx={sourceIdx} column={column} />
            : <Td key={column.dataIndex} idx={sourceIdx} column={column} />;
        })}
      </tr>
    );
  })}
  </tbody>
);

TableBody.propTypes = {
  className: PropTypes.string,
  columns: PropTypes.array,
  dataSource: PropTypes.array,
};

TableBody.defaultProps = {
  className: 'tbody',
  columns: [],
  dataSource: [],
}

export {TableBody};
