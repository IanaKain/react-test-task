import React from 'react';
import PropTypes from 'prop-types';


const TableHead = (props) => (
  <thead className="thead-light">
    <tr>
      {props.columns.map((col) => (
        <th key={col.dataIndex} scope="col">{col.title}</th>
      ))}
    </tr>
  </thead>
);

TableHead.propTypes = {
  className: PropTypes.string,
  columns: PropTypes.array,
};

TableHead.defaultProps = {
  className: 'thead',
  columns: [],
}

export {TableHead};
