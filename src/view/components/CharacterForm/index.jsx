import React from 'react';
import PropTypes from 'prop-types';
import {RadioGroup} from '../RadioGroup';
import {InputField} from '../InputField';
import {SelectField} from '../Select';
import {genders} from '../../../constants';


const CharacterFormContent = (props) => (
  <React.Fragment>
    <InputField
      id="name"
      value={props.name.value}
      error={props.name.error}
      onBlur={props.onBlur}
      onFocus={props.onFocus}
      onChange={props.onChange}
      required
    />
    <SelectField
      id="species"
      value={props.species.value}
      options={props.species.options}
      error={props.species.error}
      onBlur={props.onBlur}
      onFocus={props.onFocus}
      onChange={props.onChange}
      required
    />
    <RadioGroup
      id="gender"
      data={Object.values(genders)}
      selected={props.gender.value}
      onChange={props.onChange}
      required
    />
    <InputField
      id="homeworld"
      value={props.homeworld.value}
      error={props.homeworld.error}
      onChange={props.onChange}
    />
    <div className="form-group">
      <button
        type="submit"
        className="btn btn-primary"
        disabled={props.loading}
      >
        Submit
      </button>
    </div>
  </React.Fragment>
);

CharacterFormContent.propTypes = {
  loading: PropTypes.bool.isRequired,
  name: PropTypes.object.isRequired,
  species: PropTypes.object.isRequired,
  gender: PropTypes.object.isRequired,
  homeworld: PropTypes.object.isRequired,
  onBlur: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

export {CharacterFormContent};
