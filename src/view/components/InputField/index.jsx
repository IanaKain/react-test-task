import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {capitalize, noop} from '../../../utils';


const InputField = (props) => {
  return (
    <div className="form-group">
      <label htmlFor={props.id}>{capitalize(props.id)}</label>
      {props.required && <span className="required-mark">*</span>}
      <input
        type={props.type}
        className={classNames('form-control', {'is-invalid': props.required && props.error})}
        id={props.id}
        value={props.value}
        onFocus={props.onFocus && props.onFocus(props.id)}
        onBlur={props.onBlur && props.onBlur(props.id)}
        onChange={props.onChange(props.id)}
      />
      {props.required && <div className="invalid-feedback">{props.error}</div>}
    </div>
  )
}

InputField.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  required: PropTypes.bool,
  error: PropTypes.string,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
};

InputField.defaultProps = {
  type: 'text',
  error: '',
  required: false,
  onBlur: noop,
  onFocus: noop,
};

export {InputField};
