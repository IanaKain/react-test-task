import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';


const firstPage = 1;

const Pagination = (props) => {
  const lastPage = props.totalPagesCount / props.limit;
  const prevPageIsDisabled = props.currentPage === firstPage;
  const nextPageIsDisabled = props.currentPage === lastPage;

  const goToPage = (page) => () => {
    props.onPageChange(page);
  }

  return (
    <nav aria-label="Data grid navigation">
      <ul className="pagination justify-content-end">
        <li className={classNames('page-item', {disabled: prevPageIsDisabled})}>
          <button
            type="button"
            className="page-link"
            tabIndex="-1"
            onClick={goToPage(props.currentPage - 1)}
          >
            Previous
          </button>
        </li>
        {
          Array.from({length: lastPage}).map((_, idx) => {
            const page = idx + 1;
            const isActive = page === props.currentPage;

            return (
              <li key={page} className={classNames('page-item', {active: isActive})}>
                <button
                  type="button"
                  className="page-link"
                  onClick={goToPage(page)}
                >
                  {page}
                  {isActive && <span className="sr-only">(current)</span>}
                </button>
              </li>
            );
          })
        }
        <li className={classNames('page-item', {disabled: nextPageIsDisabled})}>
          <button
            type="button"
            className="page-link"
            onClick={goToPage(props.currentPage + 1)}
          >
            Next
          </button>
        </li>
      </ul>
    </nav>
  );
}

Pagination.propTypes = {
  totalPagesCount: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export {Pagination};
