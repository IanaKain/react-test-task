import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {capitalize, noop} from '../../../utils';


const SelectField = (props) => {
  return (
    <div className="form-group">
      <label htmlFor={props.id}>{capitalize(props.id)}</label>
      {props.required && <span className="required-mark">*</span>}
      <select
        className={classNames('form-control', {'is-invalid': props.required && props.error})}
        id={props.id}
        value={props.value}
        onFocus={props.onFocus && props.onFocus(props.id)}
        onBlur={props.onBlur && props.onBlur(props.id)}
        onChange={props.onChange(props.id)}
      >
        {props.options.map((opt, idx) => (<option key={opt+idx}>{opt}</option>))}
      </select>
      {props.required && <div className="invalid-feedback">{props.error}</div>}
    </div>
  )
}

SelectField.propTypes = {
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.string),
  value: PropTypes.string,
  error: PropTypes.string,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  required: PropTypes.bool,
};

SelectField.defaultProps = {
  options: [],
  value: '',
  error: '',
  required: false,
  onBlur: noop,
  onFocus: noop,
};

export {SelectField};
