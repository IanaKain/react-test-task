import React from 'react';
import PropTypes from 'prop-types';
import {inject, observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import {routes} from '../../../../routes';


const ListHeader = inject('charactersStore')(
  observer(
    class ListHeader extends React.Component {
      static propTypes = {
        title: PropTypes.string,
        charactersStore: PropTypes.object.isRequired,
      };

      static defaultProps = {
        title: 'List View',
      }

      onSearchChange = ({target: {value}}) => {
        this.props.charactersStore.setSearch(value);
      };

      render() {
        return (
          <React.Fragment>
            <h1>{this.props.title}</h1>
            <div className="row">
              <div className="col-sm-6">
                <div className="form-group">
                  <label htmlFor="searchInput" className="sr-only">
                    Search
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="searchInput"
                    placeholder="Search..."
                    onChange={this.onSearchChange}
                  />
                </div>
              </div>
              <div className="col-sm-6 text-sm-right">
                <button type="button" className="btn btn-primary mb-3 btn-add">
                  <Link
                    to={routes.createCharacter}
                    className="link-btn-add"
                  >
                    Add New
                  </Link>
                </button>
              </div>
            </div>
          </React.Fragment>
        );
      }
    }
  )
);

export {ListHeader};
