import React from 'react';
import PropTypes from 'prop-types';
import {observer, inject} from 'mobx-react';
import {List} from './List';
import {Pagination} from '../../components/Pagination';
import {ListHeader} from './ListHeader';
import {Link} from 'react-router-dom';
import {routes} from '../../../routes';
import {generatePath} from '../../../utils';


const Characters = inject('charactersStore')(
  observer(
    class Characters extends React.Component {
      static propTypes = {
        charactersStore: PropTypes.object.isRequired,
      }

      getColumns = () => {
        return [
          {
            title: 'Id',
            dataIndex: 'id',
          },
          {
            title: 'Name',
            dataIndex: 'name',
          },
          {
            title: 'Species',
            dataIndex: 'species',
          },
          {
            title: 'Gender',
            dataIndex: 'gender',
          },
          {
            title: 'Homeworld',
            dataIndex: 'homeworld',
          },
          {
            title: 'Actions',
            dataIndex: 'null',
            render: (_, item) => (
              <div className="btn-group btn-group-sm" role="group" aria-label="Actions">
                <button
                  type="button"
                  className="btn btn-secondary"
                >
                  <Link
                    className="link-btn-edit"
                    to={{
                      pathname: generatePath(routes.editCharacter, {id: item.id}),
                      state: {character: JSON.stringify(item)}
                    }}
                  >
                    <i className="fa fa-pencil" aria-hidden="true" /> Edit
                  </Link>
                </button>
                <button
                  type="button"
                  className="btn btn-danger"
                  onClick={() => this.props.charactersStore.removeCharacter(item.id)}
                >
                  <i className="fa fa-trash-o" aria-hidden="true" /> Remove
                </button>
              </div>
            )
          },
        ];
      };

      onPageChange = (page) => {
        this.props.charactersStore.goToPage(page);
      }

      render() {
        const {totalPagesCount, characters, defaultParams, currentPage} = this.props.charactersStore;

        return (
          <div className="App">
            <div className="container">
              <ListHeader/>
              {
                characters && characters.length
                  ? <List columns={this.getColumns()} dataSource={characters}/>
                  : <div>No Results Found</div>
              }
              {
                totalPagesCount
                  ? (
                    <Pagination
                      limit={defaultParams._limit}
                      totalPagesCount={totalPagesCount}
                      currentPage={currentPage}
                      onPageChange={this.onPageChange}
                    />
                  )
                  : null
              }
            </div>
          </div>
        );
      }
    }
  )
);

export {Characters};
