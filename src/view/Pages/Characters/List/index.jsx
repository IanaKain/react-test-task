import React from 'react';
import PropTypes from 'prop-types';
import {TableHead} from '../../../components/TableHead';
import {TableBody} from '../../../components/TableBody';


class List extends React.Component {
  static propTypes = {
    dataSource: PropTypes.arrayOf(PropTypes.object),
    columns: PropTypes.arrayOf(PropTypes.object),
  };

  static defaultProps = {
    dataSource: [],
    columns: [],
  };

  collectData = () => {
    return this.props.columns.map((column, idx) => {
      const key = column.dataIndex;
      const data = this.props.dataSource.map((source) => {
        return {source, target: source[key] ? source[key] : null}
      });

      return {...column, data}
    });
  };

  render() {
    return (
      <table className="table table-bordered table-hover">
        <TableHead
          className="thead-light"
          columns={this.props.columns}
        />
        <TableBody
          columns={this.collectData()}
          dataSource={this.props.dataSource}
        />
      </table>
    );
  }
}

export {List};
