import React from 'react';
import PropTypes from 'prop-types';
import {observer, inject} from 'mobx-react';
import {CharacterFormContent} from '../../components/CharacterForm';
import {defaultLoader, genders} from '../../../constants';
import {routes} from '../../../routes';


const CreateEditCharacter = inject('charactersStore', 'speciesStore')(
  observer(
    class CreateEditCharacter extends React.Component {
      constructor(props) {
        super(props);
        this.formRef = React.createRef();
        this.state = {
          name: {value: '', required: true},
          species: {value: undefined, required: true},
          gender: {value: genders.male, required: true},
          homeworld: {value: '', required: false},
          formSubmitted: false,
        };
      }

      static propTypes = {
        charactersStore: PropTypes.object.isRequired,
        speciesStore: PropTypes.object.isRequired,
      };

      componentDidMount() {
        if (this.forType.edit) {
          const character = JSON.parse(this.props.location.state.character);
          const {id, ...restFields} = Object.entries(character)
            .reduce((newState, [key, value]) => ({...newState, [key]: {...this.state[key], value}}), {});

          this.setState(restFields);
        }
      }

      componentDidUpdate(prevProps, prevState, snapshot) {
        if (!this.state.species.value && this.props.speciesStore.species.length) {
          this.setState((prevState) => ({
            species: {
              ...prevState.species,
              value: this.props.speciesStore.species[0]
            }
          }));
        }
      }

      get forType() {
        return {
          create: Boolean(this.props.location.pathname === routes.createCharacter),
          edit: Boolean(this.props.match.params.id && this.props.location.state),
        };
      }

      onChange = (key) => ({target}) => {
        this.setState((prevState) => ({
          [key]: {...prevState[key], value: target.value}
        }));
      };

      onBlur = (key) => () => {
        if (this.state[key].required && !this.state[key].value) {
          this.setFieldError(key);
        }
      };

      onFocus = (key) => () => {
        this.setState((prevState) => ({
          [key]: {...prevState[key], error: ''}
        }));
      };

      setFieldError = (field) => {
        this.setState((prevState) => ({
          [field]: {...prevState[field], error: 'This field is required'}
        }))
      };

      isFormValid = () => {
        const {formSubmitted, ...fields} = this.state;

        return Object.entries(fields).map(([key, data]) => {
          if (data.required && !data.value) {
            this.setFieldError(key);

            return false;
          }

          return true;
        }).every(validation => validation);
      };

      onSubmit = (e) => {
        e.preventDefault();

        if (this.isFormValid()) {
          const {formSubmitted, ...fields} = this.state;
          const data = Object.keys(fields).reduce((acc, key) => ({
            ...acc, [key]: fields[key].value
          }), {});

          if (this.forType.create) {
            this.createCharacter(data);
          }

          if (this.forType.edit) {
            this.editCharacter(data);
          }
        } else {
          this.setState({formSubmitted: true}, () => {
            const firstInvalid = Array.from(this.formRef.current.elements)
              .find((field) => field.className.includes('is-invalid'));

            if (firstInvalid) {
              this.setState({formSubmitted: false});
              firstInvalid.focus();
            }
          })
        }
      };

      createCharacter = (data) => {
        this.props.charactersStore.addCharacter(data);
      };

      editCharacter = (data) => {
        this.props.charactersStore.editCharacter({
          id: this.props.match.params.id,
          body: data,
        });
      }

      render() {
        return (
          <div className="form-create-character">
            <div className="container">
              <form onSubmit={this.onSubmit} ref={this.formRef}>
                <CharacterFormContent
                  loading={this.props.charactersStore.loading === defaultLoader.pending}
                  name={this.state.name}
                  species={{...this.state.species, options: this.props.speciesStore.species}}
                  gender={this.state.gender}
                  homeworld={this.state.homeworld}
                  onBlur={this.onBlur}
                  onFocus={this.onFocus}
                  onChange={this.onChange}
                />
              </form>
            </div>
          </div>
        )
      }
    }
  )
);

export {CreateEditCharacter};
