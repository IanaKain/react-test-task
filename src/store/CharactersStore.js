import {action, computed, observable, autorun, reaction, decorate} from 'mobx';
import {api} from '../api';
import {debounce, JSONToSearch, searchToJSON} from '../utils';
import {defaultLoader} from '../constants';
import {routes} from '../routes';


class CharactersStore {
  localUrl = window.location.origin;
  baseEndpoint = routes.characters;
  defaultParams = {
    _page: 1,
    _limit: 10,
  };
  search = undefined;
  loading = defaultLoader.none;
  currentPage = this.defaultParams._page;
  meta = null;
  characters = null;
  debouncedSearch = debounce(() => {
    this.fetchCharacters();
  }, 200);

  constructor() {
    autorun(() => {
      this.fetchCharacters(this.defaultParams);
    });
    reaction(
      () => this.currentPage,
      () => {
        this.fetchCharacters({
          ...this.defaultParams,
          _page: this.currentPage,
        })
      }
    );
    reaction(
      () => this.search,
      () => { this.debouncedSearch()}
    );
  };

  get totalPagesCount() {
    const pagination = this.meta && this.meta.pagination;

    if (pagination && pagination.length) {
      const lastPage = pagination[pagination.length -1];

      return this.defaultParams._limit * Number(searchToJSON(lastPage)._page);
    }

    return 0;
  };

  setCharacters = (characters) => {
    this.characters = characters;
  };

  setMeta = (meta) => {
    this.meta = meta;
  };

  setLoader = (type) => {
    this.loading = type;
  };

  goToPage = (page) => {
    this.currentPage = page;
  };

  setSearch = (value) => {
    this.search = value;
  }

  fetchCharacters = (params = {}) => {
    this.setLoader(defaultLoader.pending);

    if (this.search) {
      const query = JSONToSearch({q: this.search});

      api.get({url: `${this.baseEndpoint}?${query}`})
        .then(({data, meta}) => {
          this.setCharacters(data);
          this.setMeta(meta);
          this.setLoader(defaultLoader.completed);
        })
        .catch(error => {
          this.setLoader(defaultLoader.error);
        });

      return;
    }

    const query = JSONToSearch({
      _page: params._page || this.defaultParams._page,
      _limit: params._limit || this.defaultParams._limit,
    });

    api.get({url: `${this.baseEndpoint}?${query}`})
      .then(({data, meta}) => {
        if (!data.length && this.currentPage > 1) {
          this.goToPage(this.currentPage - 1);
        }
        this.setCharacters(data);
        this.setMeta(meta);
        this.setLoader(defaultLoader.completed);
      })
      .catch(error => {
        this.setLoader(defaultLoader.error);
      });
  };

  addCharacter = (body) => {
    this.setLoader(defaultLoader.pending);

    api.post({url: this.baseEndpoint, body})
      .then(({data}) => {
        if (!data.id) {
          throw new Error('Error while adding character');
        }
        setTimeout(() => {
          this.setLoader(defaultLoader.completed);
        }, 1000);
        window.location.href = `${this.localUrl}${this.baseEndpoint}`;
      })
      .catch(error => {
        this.setLoader(defaultLoader.error);
      });
  };

  editCharacter = ({id, body}) => {
    this.setLoader(defaultLoader.pending);

    api.patch({url: `${this.baseEndpoint}/${id}`, body})
      .then(({data}) => {
        if (!data.id) {
          throw new Error('Error while editing character');
        }
        setTimeout(() => {
          this.setLoader(defaultLoader.completed);
        }, 1000);
        window.location.href = `${this.localUrl}${this.baseEndpoint}`;
      })
      .catch(error => {
        this.setLoader(defaultLoader.error);
      });
  };

  removeCharacter = (id) => {
    this.setLoader(defaultLoader.pending);

    api.delete({url: `${this.baseEndpoint}/${id}`})
      .then(response => {
        this.setLoader(defaultLoader.completed);
        this.fetchCharacters({
          ...this.defaultParams,
          _page: this.currentPage
        });
      })
      .catch(error => {
        this.setLoader(defaultLoader.error);
      });
  };
}

decorate(CharactersStore, {
  loading: observable,
  currentPage: observable,
  search: observable,
  meta: observable,
  characters: observable,
  totalPagesCount: computed,
  setCharacters: action,
  setMeta: action,
  setLoader: action,
  fetchCharacters: action,
  addCharacter: action,
  editCharacter: action,
  removeCharacter: action,
  goToPage: action,
  setSearch: action,
})

const charactersStore = new CharactersStore();
export {charactersStore};
