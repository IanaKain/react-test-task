import {action, autorun, decorate, observable} from 'mobx';
import {api} from '../api';
import {defaultLoader} from '../constants';
import {routes} from '../routes';


class SpeciesStore {
  baseEndpoint = routes.species;
  species = [];
  loading = defaultLoader.none;

  constructor() {
    autorun(() => {
      this.fetchSpecies()
    });
  }

  setLoader = (type) => {
    this.loading = type;
  };

  setSpecies = (species) => {
    this.species = species;
  };

  fetchSpecies = () => {
    this.setLoader(defaultLoader.pending);

    api.get({url: `${this.baseEndpoint}`})
      .then(({data}) => {
        this.setSpecies(data);
        this.setLoader(defaultLoader.completed);
      })
      .catch(error => {
        this.setLoader(defaultLoader.error);
      })
  };
}

decorate(SpeciesStore, {
  species: observable,
  loading: observable,
  setLoader: action,
  setSpecies: action,
  fetchSpecies: action,
})

const speciesStore = new SpeciesStore();
export {speciesStore};
