import {createBrowserHistory} from 'history';


class AppStore {
  constructor() {
    this.history = createBrowserHistory();
  }
}

const appStore = new AppStore();
export {appStore};
