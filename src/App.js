import React from 'react';
import {Route, Switch} from 'react-router-dom';

import {routes} from './routes';
import {About} from './view/Pages/About';
import {Characters} from './view/Pages/Characters';
import {CreateEditCharacter} from './view/Pages/CreateEditCharacter';
import {Navigation} from './view/components/Navigation';


class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Navigation/>
        <Switch>
          <Route exact path={routes.createCharacter} component={CreateEditCharacter}/>
          <Route exact path={routes.editCharacter} component={CreateEditCharacter}/>
          <Route exact path={routes.characters} component={Characters}/>
          <Route path={routes.root} component={About}/>
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
