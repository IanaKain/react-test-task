context('sonalake-task-react App', () => {
  beforeEach(() => {
    cy.visit('');
  });

  it('should display the title', () => {
    cy.get('.nav-link').should('have.text', 'List View');
  });
});
